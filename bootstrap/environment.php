<?php

use Dotenv\Dotenv;

$dotenv = Dotenv::createImmutable(BASE_DIR);
$dotenv->load();

$dotenv->required('app_debug')->isInteger();
$dotenv->required('db_hostname');
$dotenv->required('db_port')->isInteger();
$dotenv->required('db_database');
$dotenv->required('db_username');
$dotenv->required('db_password');