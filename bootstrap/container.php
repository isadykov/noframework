<?php

use Laminas\Diactoros\Response;
use Laminas\Diactoros\ServerRequestFactory;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

return [
    RequestInterface::class => function(ContainerInterface $c) {
        return ServerRequestFactory::fromGlobals();
    },
    ResponseInterface::class => function(ContainerInterface $c) {
        return new Response();
    },
];
