<?php

/**
* @var FastRoute\RouteCollector $r
*/

$r->get('/', [App\Controllers\IndexController::class, 'index']);
$r->get('/test', [App\Controllers\IndexController::class, 'test']);