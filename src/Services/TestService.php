<?php

namespace App\Services;

use Doctrine\DBAL\DriverManager;

class TestService
{
    protected $connection;
    protected $builder;

    public function __construct()
    {
        $this->connection = DriverManager::getConnection([
            'dbname'   => $_ENV['db_database'],
            'user'     => $_ENV['db_username'],
            'password' => $_ENV['db_password'],
            'host'     => $_ENV['db_hostname'],
            'port'     => $_ENV['db_port'],
            'driver'   => 'pdo_mysql',
        ]);
        $this->builder = $this->connection->createQueryBuilder();
    }

    public function handle()
    {
        $query = $this->builder->select('*')->from('users');
        return $query->execute()->fetchAllAssociative();
    }
}