<?php

namespace App\Controllers;

use Twig\Environment;
use Twig\Loader\FilesystemLoader;

abstract class Controller
{
    public function render(string $template, ?array $vars=null) : string
    {
        $loader = new FilesystemLoader(BASE_DIR . '/templates');
        $twig = new Environment($loader);
        return $twig->render($template, $vars);
    }
}