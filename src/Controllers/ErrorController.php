<?php

namespace App\Controllers;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class ErrorController
{
    public function notFound(
        RequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $response->withStatus(404);
        $response->getBody()->write('404 not found');
        return $response;
    }

    public function methodNotAllowed(
        RequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $response->withStatus(405);
        $response->getBody()->write('405 method not allowed');
        return $response;
    }

    public function unexpected(
        RequestInterface $request,
        ResponseInterface $response
    ) : ResponseInterface {
        $response->withStatus(503);
        $response->getBody()->write('503 Internal server error');
        return $response;
    }
}
