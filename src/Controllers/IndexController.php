<?php

namespace App\Controllers;

use App\Services\TestService;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class IndexController extends Controller
{
    protected $testService;

    public function __construct(TestService $testService)
    {
        $this->testService = $testService;
    }

    public function index(
        RequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $html = $this->render('index.html.twig', [
            'message' => 'Hello, world!',
        ]);
        $response->getBody()->write($html);
        return $response;
    }

    public function test(
        RequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $data = $this->testService->handle();
        $response->getBody()->write('<pre>' . json_encode($data) . '</pre>');
        return $response;
    }
}