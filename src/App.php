<?php

namespace App;

use App\Controllers\ErrorController;
use FastRoute\Dispatcher;
use FastRoute\RouteCollector;
use Laminas\HttpHandlerRunner\Emitter\SapiEmitter;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

use function FastRoute\simpleDispatcher; 

class App
{
    protected $request;
    protected $container;
    protected $emitter;


    public function __construct(
        RequestInterface $request,
        ContainerInterface $container,
        SapiEmitter $emitter
    ) {
        $this->request = $request;
        $this->container = $container;
        $this->emitter = $emitter;
    }

    public function run()
    {
        $dispatcher = $this->createRouteDispatcher();
        $response = $this->handleRequest($dispatcher);
        $this->emitResponse($response);
        die();
    }

    private function createRouteDispatcher() : Dispatcher
    {
        return simpleDispatcher(function(RouteCollector $r) {
            include BASE_DIR . '/bootstrap/routes.php';
        });
    }

    private function handleRequest(Dispatcher $dispatcher) : ResponseInterface
    {
        $routeInfo = $dispatcher->dispatch(
            $this->request->getMethod(),
            $this->request->getUri()->getPath()
        );
        return $this->getResponseForRoute($routeInfo);
    }

    private function getResponseForRoute(array $routeInfo) : ResponseInterface
    {
        switch ($routeInfo[0]) {
            case Dispatcher::NOT_FOUND:
                return $this->container->call([ErrorController::class, 'notFound']);
                break;
            case Dispatcher::METHOD_NOT_ALLOWED:
                return $this->container->call([ErrorController::class, 'methodNotAllowed']);
                break;
            case Dispatcher::FOUND:
                $handler = $routeInfo[1];
                $vars = $routeInfo[2];
                return $this->container->call($handler, $vars);
                break;
            default:
                return $this->container->call([ErrorController::class, 'unexpected']);
        }
    }

    private function emitResponse(ResponseInterface $response) : void
    {
        $this->emitter->emit($response);
    }
}