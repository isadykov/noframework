# noframework

* `php-di/php-di` - dependency injection
* `nikic/fast-route` - routing
* `laminas/diactoros` - PSR7 request response implementations
* `laminas-httphandlerrunner` - response emitter
* `doctrine/dbal` - database abstraction
* `symfony/var-dumper` - debugging variables
* `twig/twig` - template engine

# todo
* `vlucas/phpdotenv` - dotenv
