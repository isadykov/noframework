<?php

define('BASE_DIR', dirname(__DIR__));

require_once BASE_DIR . '/vendor/autoload.php';

// Код внутри анонимной функции, чтобы в глобальной видимости ничего не было
(function() {
    // Заполняем $_ENV данными из файла .env
    require_once BASE_DIR . '/bootstrap/environment.php';

    // Создаем контейнер инъекции зависимостей
    $builder = new DI\ContainerBuilder();
    $builder->useAutowiring(true);
    $builder->useAnnotations(false);
    $builder->addDefinitions(include BASE_DIR . '/bootstrap/container.php');

    $container = $builder->build();

    $app = $container->get(\App\App::class);
    $app->run();
})();